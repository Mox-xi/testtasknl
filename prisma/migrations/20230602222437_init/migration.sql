-- CreateEnum
CREATE TYPE "Action" AS ENUM ('BUY', 'SELL', 'REFILL');

-- CreateTable
CREATE TABLE "Users" (
    "id" INTEGER NOT NULL,
    "balance" INTEGER DEFAULT 0,

    CONSTRAINT "Users_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "BalanceHistory" (
    "id" INTEGER NOT NULL,
    "userId" INTEGER NOT NULL,
    "action" "Action" NOT NULL,
    "amount" INTEGER NOT NULL,
    "createdAt" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "BalanceHistory_pkey" PRIMARY KEY ("id")
);
