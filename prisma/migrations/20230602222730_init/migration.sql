-- AlterTable
CREATE SEQUENCE balancehistory_id_seq;
ALTER TABLE "BalanceHistory" ALTER COLUMN "id" SET DEFAULT nextval('balancehistory_id_seq');
ALTER SEQUENCE balancehistory_id_seq OWNED BY "BalanceHistory"."id";
