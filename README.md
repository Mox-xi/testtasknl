# Тестовое задание

## Запуск
```shell
# Отредактируйте файл конфигурации
cp ./.env.sample ./.env

docker-compose up 
```

### Server
Использует порт 4000  

Эндпоинты:

`POST /api/items` - body: {
appId?: number,
currency?: string
} - Метод возвращает модифицированный список предметов skinport, кешируя результат.

`POST /api/create` - body: {
 userId: number
} - Метода возвращает созданного пользователя.

`POST /api/get` - body: {
userId: number
} - Метода возвращает данные пользователя.

`POST /api/action` - body: {
    userId: number,
    action: 'BUY' | 'SELL' | 'REFILL',
    amount: number
} - Метод фиксирует изменение баланса, с последующим обновлением.
