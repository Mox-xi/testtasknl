import { Controller, Post, Body, HttpCode } from '@nestjs/common';
import { AppService } from './app.service';

export interface IActionInput {
  userId: number;
  amount: number;
  action: string;
}

@Controller('api')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('items')
  @HttpCode(200)
  items(@Body() input: { appId: number; currency: string }) {
    return this.appService.items(input.appId, input.currency);
  }

  @Post('action')
  @HttpCode(200)
  action(@Body() input: IActionInput) {
    return this.appService.action(input);
  }

  @Post('create')
  @HttpCode(200)
  create(@Body() body: { userId: number }) {
    return this.appService.createUser(body.userId);
  }

  @Post('get')
  @HttpCode(200)
  get(@Body() body: { userId: number }) {
    return this.appService.getUser(body.userId);
  }
}
