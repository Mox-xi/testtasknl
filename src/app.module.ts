import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaService } from '../prisma/prisma.service';
import { UsersService } from './services/users.service';
import { RedisService } from './services/redis.service';
import { BalanceHistoryService } from './services/balanceHistory.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [
    AppService,
    PrismaService,
    UsersService,
    BalanceHistoryService,
    RedisService,
  ],
})
export class AppModule {}
