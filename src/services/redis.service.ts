import { Injectable } from '@nestjs/common';
import { createClient } from 'redis';

@Injectable()
export class RedisService {
  public redisClient: any;

  constructor() {
    this.redisClient = createClient({
      url: process.env.REDIS,
    });
    this.redisClient.connect();
  }

  async set(key, data) {
    await this.redisClient.set(key, JSON.stringify(data), 'EX', 300);
  }

  async get(key) {
    const data = await this.redisClient.get(key);
    return data ? JSON.parse(data) : null;
  }
}
