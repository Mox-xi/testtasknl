import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma/prisma.service';

@Injectable()
export class UsersService {
  constructor(private prisma: PrismaService) {}

  async createUser(userId: number) {
    return this.prisma.users.create({
      data: {
        id: userId,
        balance: 0,
      },
    });
  }

  async getUser(userId: number) {
    return this.prisma.users.findFirst({
      where: {
        id: userId,
      },
    });
  }

  async checkUserExist(userId: number) {
    const user = await this.prisma.users.findFirst({
      where: {
        id: userId,
      },
    });

    return !!user;
  }

  async updateBalanceByUserId(userId: number, newBalance: number) {
    await this.prisma.users.update({
      where: {
        id: userId,
      },
      data: {
        balance: newBalance,
      },
    });
  }
}
