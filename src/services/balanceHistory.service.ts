import { PrismaService } from '../../prisma/prisma.service';
import { Injectable } from '@nestjs/common';

export enum Actions {
  BUY = 'BUY',
  SELL = 'SELL',
  REFILL = 'REFILL',
}

@Injectable()
export class BalanceHistoryService {
  constructor(private prisma: PrismaService) {}

  async createBalanceHistoryItem(data) {
    return this.prisma.balanceHistory.create({
      data,
    });
  }

  getAllByUserId(userId: number) {
    return this.prisma.balanceHistory.findMany({
      where: {
        userId,
      },
    });
  }

  async calculateBalanceByUserId(userId: number) {
    const balanceHistoryItems = await this.getAllByUserId(userId);
    let actualBalance = 0;
    for (const item of balanceHistoryItems) {
      if (item.action === Actions.BUY) {
        actualBalance -= item.amount;
      }

      if (item.action === Actions.SELL) {
        actualBalance += item.amount;
      }

      if (item.action === Actions.REFILL) {
        actualBalance += item.amount;
      }
    }
    return actualBalance;
  }
}
