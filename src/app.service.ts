import {
  Actions,
  BalanceHistoryService,
} from './services/balanceHistory.service';
import { UsersService } from './services/users.service';
import { RedisService } from './services/redis.service';
import { IActionInput } from './app.controller';
import { Injectable } from '@nestjs/common';
import axios from 'axios';

@Injectable()
export class AppService {
  constructor(
    private readonly balanceHistoryService: BalanceHistoryService,
    private readonly usersService: UsersService,
    private readonly redisService: RedisService,
  ) {}

  async items(appId = 730, currency = 'EUR') {
    const itemsMap = new Map();
    let result = [];

    const cache = await this.redisService.get(`${appId}_${currency}`);

    if (!cache) {
      const noTrade = await axios
        .get(
          `https://api.skinport.com/v1/items?app_id=${appId}&currency=${currency}&tradable=0`,
        );

      for (const item of noTrade.data) {
        itemsMap.set(item.market_hash_name, item);
      }

      const trade = await axios
        .get(
          `https://api.skinport.com/v1/items?app_id=${appId}&currency=${currency}&tradable=1`,
        );
      
      for (const item of trade.data) {
        const noTrade = itemsMap.get(item.market_hash_name);
        result.push({
          ...noTrade,
          tradeInfo: {
            min_price: item.min_price,
            max_price: item.max_price,
            mean_price: item.mean_price,
            median_price: item.median_price,
          },
        });
      }

      await this.redisService.set(`${appId}_${currency}`, result);
    } else {
      result = cache;
    }

    return result;
  }

  async action(input: IActionInput) {
    const isUser = await this.usersService.checkUserExist(input.userId);

    if (isUser) {
      if (input.action === Actions.SELL) {
        await this.balanceHistoryService.createBalanceHistoryItem(input);
      }

      if (input.action === Actions.BUY) {
        await this.balanceHistoryService.createBalanceHistoryItem(input);
      }

      if (input.action === Actions.REFILL) {
        await this.balanceHistoryService.createBalanceHistoryItem(input);
      }

      const balance = await this.balanceHistoryService.calculateBalanceByUserId(
        input.userId,
      );
      await this.usersService.updateBalanceByUserId(input.userId, balance);

      return `userId: ${input.userId} balance updated`;
    }

    return;
  }

  createUser(userId) {
    return this.usersService.createUser(userId);
  }

  getUser(userId) {
    return this.usersService.getUser(userId);
  }
}
