FROM node:19-bullseye-slim

WORKDIR /app

COPY ./ ./

RUN npm install && npm run build

CMD ["/bin/bash", "-c", "npx prisma generate;npx prisma migrate dev; npm run start"]
